#include <multiCameraIrControl.h> // Project Page - http://sebastian.setz.name/arduino/my-libraries/multi-Camera-IR-Control/

Canon camera(2); // 2 is the digital pin driving the LED, this should be connected to the positive leg.
             // D5 is the library module that is compatible with my Canon Rebel T1i. 
             // Models supported at the time of writing this are Canon, Nikon, Sony, Minolta, 
             // Olympus, and Pentax.

void setup(){
}

void loop(){ // This will repeat over and over
  camera.shutterNow(); // Trigger the shutter
  delay(1000); // Wait for 1000 ms (1s) before proceeding
}




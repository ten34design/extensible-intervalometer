#include <multiCameraIrControl.h> // Project Page - http://sebastian.setz.name/arduino/my-libraries/multi-Camera-IR-Control/

Canon camera(2); // 2 is the digital pin driving the LED, this should be connected to the positive leg.
             // D5 is the library module that is compatible with my Canon Rebel T1i. 
             // Models supported at the time of writing this are Canon, Nikon, Sony, Minolta, 
             // Olympus, and Pentax.

// Map the buttons
const int buttonOne = 8; // Up
const int buttonTwo = 9; // Down
const int buttonThree = 10; // Cycle
int button_state = 0;

void setup(){
  pinMode(buttonOne, INPUT); // Set the buttons as inputs
  pinMode(buttonTwo, INPUT);
  pinMode(buttonThree, INPUT);
  digitalWrite(buttonOne, HIGH); // Set the input to use a pullup resistor
  digitalWrite(buttonTwo, HIGH);
  digitalWrite(buttonThree, HIGH);
}

void loop(){ // This will repeat over and over
  button_state = read_pins(); // Poll the button pins using the new function read_pins()
  if (button_state != 0){ // If a button has been pressed, proceed
    delay(button_state*500); // Multiply the button number by a half second (500ms) for testing
    camera.shutterNow(); // Trigger the shutter
  }
}

int read_pins(){ // This function tells us if a button has been pressed, and which button
  if(digitalRead(buttonOne)==0){return 1;
  }else if(digitalRead(buttonTwo)==0){return 2;
  }else if(digitalRead(buttonThree)==0){return 3;
  }else{return 0;}
}


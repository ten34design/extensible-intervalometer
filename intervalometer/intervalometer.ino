#include <multiCameraIrControl.h> // Project Page - http://sebastian.setz.name/arduino/my-libraries/multi-Camera-IR-Control/
#include <Adafruit_GFX.h> // Project Page - https://github.com/adafruit/Adafruit-PCD8544-Nokia-5110-LCD-library
#include <Adafruit_PCD8544.h> // Project Page - https://github.com/adafruit/Adafruit-GFX-Library

// Nokia 5110 LCD Module pin mapping
// pin 7 - CLK (Clock)
// pin 6 - Din (Serial Data)
// pin 5 - DC (Data/Command Select)
// pin 4 - CE (LCD Chip Enable)
// pin 3 - RST (LCD Reset)
Adafruit_PCD8544 display = Adafruit_PCD8544(7, 6, 5, 4, 3);

Canon camera(2); // 2 is the digital pin driving the LED, this should be connected to the positive leg.
             // D5 is the library module that is compatible with my Canon Rebel T1i. 
             // Models supported at the time of writing this are Canon, Nikon, Sony, Minolta, 
             // Olympus, and Pentax.

// Map the buttons
const int buttonOne = 8; // Up
const int buttonTwo = 9; // Down
const int buttonThree = 10; // Cycle
int button_state[1];
int cur_page = -1;
int cur_selected_page = 0;
int cur_selected_option = 0;
int inByte;

int menu_items = 5;
char* menu_item[][5]={
  {
  "I'VALOMETER", // Run Page
  "MODE", // 
  "TRIGGER", //
  "SETUP", // 
  "MISC", // Extra Page
  },
};

int menu_item_height[][5]={
  {
    1,
    10,
    19,
    28,
    37,
  },
};
    

void setup(){
  Serial.begin(9600);
  // Set up buttons
  pinMode(buttonOne, INPUT); // Set the buttons as inputs
  pinMode(buttonTwo, INPUT);
  pinMode(buttonThree, INPUT);
  digitalWrite(buttonOne, HIGH); // Set the input to use a pullup resistor
  digitalWrite(buttonTwo, HIGH);
  digitalWrite(buttonThree, HIGH);
  //Set up display
  display.begin();
  display.setContrast(50);
  display.clearDisplay();
   
  // Write display
  //writestring(17, 2, menu_item[0][0], BLACK); // Write first menu item on display
  //display.drawRect(0, 0, 84, 11, BLACK); // Draw a rectangle around text
  drawMenu(); 
}

void loop(){ // This will repeat over and over
  if (Serial.available() > 0) {
    inByte = Serial.read();
    if(inByte == 119){
      Serial.println("UP");
      menu_up();
    }else if(inByte == 115){
      Serial.println("DOWN");
      menu_down();
    }else if(inByte == 100){
      Serial.println("ENTER");
      select_button();
    }else{
      Serial.println(inByte);
    }
  }
}

void drawBackButton(uint8_t color){
  if (color == WHITE){
    display.fillRect(1,1,14,9,BLACK);
  }else{
    display.fillRect(1,1,14,9,WHITE);
  }
  display.drawLine(3, 5, 5, 3, color);
  display.drawLine(3, 5, 5, 7, color);
  display.drawLine(3, 5, 10, 5, color);
  display.drawLine(13, 1, 13, 9, BLACK);
  display.display();
}

void drawForwardButton(uint8_t color, int x, int y){
  if (color == WHITE){
    display.fillRect(x,y,14,9,BLACK);
  }else{
    display.fillRect(x,y,14,9,WHITE);
  }
  display.drawLine(x+9, y+4, x+7, y+2, color);
  display.drawLine(x+9, y+4, x+7, y+6, color);
  display.drawLine(x+2, y+4, x+9, y+4, color);
  display.display();
}

void clearForwardButton(uint8_t color, int x, int y){
    display.fillRect(x,y,14,9,color);
  display.display();
}

void drawMenu(){
  for (int i=0; i<menu_items; i++){
    writestring(16, 2+i*9, menu_item[0][i], BLACK);
  }
  drawForwardButton(BLACK,1,menu_item_height[0][cur_selected_option]);
  display.drawRect(0, 0, 84, 48, BLACK);
  display.display();
}

int read_pins(){ // This function tells us if a button has been pressed, and which button
  if(digitalRead(buttonOne)==0){return 1;
  }else if(digitalRead(buttonTwo)==0){return 2;
  }else if(digitalRead(buttonThree)==0){return 3;
  }else{return 0;}
}

void drawchar(int x,int y,char ch, uint8_t color) {
  if (color == WHITE){
    display.fillRect(x,y,5,6,BLACK);
  }else{
    display.fillRect(x,y,5,6,WHITE);
  }
  display.setTextSize(1);
  display.setTextColor(color);
  display.setCursor(x,y);
  display.write(ch);  
}

void writestring(int x, int y, char string[], uint8_t color){
  for(int i=0; i<strlen(string); i+=1){
    drawchar(x+6*i,y,string[i],color);
  }
}

void menu_up(){
  clearForwardButton(WHITE, 1, menu_item_height[0][cur_selected_option]);
  if(cur_selected_option == 0){
    cur_selected_option = menu_items-1;
  }else{
    cur_selected_option = cur_selected_option-1;
  }
  drawForwardButton(BLACK,1,menu_item_height[0][cur_selected_option]);
}

void menu_down(){
  clearForwardButton(WHITE, 1, menu_item_height[0][cur_selected_option]);
  if(cur_selected_option == menu_items-1){
    cur_selected_option = 0;
  }else{
    cur_selected_option = cur_selected_option+1;
  }
  drawForwardButton(BLACK,1,menu_item_height[0][cur_selected_option]);
}

void select_button(){
  if (cur_page == -1){ //-1 is menu
    if(cur_selected_option == 0){
      drawchar(75,40,'0', BLACK);
      display.display();
      //goto intervalometer home page
    }else if(cur_selected_option == 1){
      drawchar(75,40,'1', BLACK);
      display.display();
      //goto mode select
    }else if(cur_selected_option == 2){
      drawchar(75,40,'2', BLACK);
      display.display();
      //goto trigger setup
    }else if(cur_selected_option == 3){
      drawchar(75,40,'3', BLACK);
      display.display();
      //goto setup
    }else if(cur_selected_option == 4){
      drawchar(75,40,'4', BLACK);
      display.display();
      //goto misc page
    }
  
  }else if(cur_page == 2){
    if(cur_selected_option == 0){
      //
    }
  }
}

#include <multiCameraIrControl.h> // Project Page - http://sebastian.setz.name/arduino/my-libraries/multi-Camera-IR-Control/
#include <Adafruit_GFX.h> // Project Page - https://github.com/adafruit/Adafruit-PCD8544-Nokia-5110-LCD-library
#include <Adafruit_PCD8544.h> // Project Page - https://github.com/adafruit/Adafruit-GFX-Library

// Nokia 5110 LCD Module pin mapping
// pin 7 - CLK (Clock)
// pin 6 - Din (Serial Data)
// pin 5 - DC (Data/Command Select)
// pin 4 - CE (LCD Chip Enable)
// pin 3 - RST (LCD Reset)
Adafruit_PCD8544 display = Adafruit_PCD8544(7, 6, 5, 4, 3);

Canon camera(2); // 2 is the digital pin driving the LED, this should be connected to the positive leg.
             // D5 is the library module that is compatible with my Canon Rebel T1i. 
             // Models supported at the time of writing this are Canon, Nikon, Sony, Minolta, 
             // Olympus, and Pentax.

// Map the buttons
const int buttonOne = 8; // Up
const int buttonTwo = 9; // Down
const int buttonThree = 10; // Cycle
int button_state[1];

char* menu_item[][4]={
  {
  "I'VALOMETER", // Trigger Delay
  "MODE", // Number of shots per program cycle
  "TRIGGER", // Trigger (high, low, none)
  "SETUP", // Trigger point (0-1023)
  },
};

void setup(){
  // Set up buttons
  pinMode(buttonOne, INPUT); // Set the buttons as inputs
  pinMode(buttonTwo, INPUT);
  pinMode(buttonThree, INPUT);
  digitalWrite(buttonOne, HIGH); // Set the input to use a pullup resistor
  digitalWrite(buttonTwo, HIGH);
  digitalWrite(buttonThree, HIGH);
  
  //Set up display
  display.begin();
  display.setContrast(40);
  display.clearDisplay();
   
  // Test display
  writestring(17, 2, menu_item[0][0], BLACK); // Write first menu item on display
  display.drawRect(0, 0, 84, 11, BLACK); // Draw a rectangle around text
  drawBackButton(WHITE); // Build a back button
  display.display();
 
}

void loop(){ // This will repeat over and over
  drawBackButton(WHITE); // This loop flashes the back button
  delay(1000);
  drawBackButton(BLACK);
  delay(1000);
}

void drawBackButton(uint8_t color){
  if (color == WHITE){
    display.fillRect(1,1,14,9,BLACK);
  }else{
    display.fillRect(1,1,14,9,WHITE);
  }
  display.drawLine(3, 5, 5, 3, color);
  display.drawLine(3, 5, 5, 7, color);
  display.drawLine(3, 5, 10, 5, color);
  display.drawLine(13, 1, 13, 9, BLACK);
  display.display();
}

int read_pins(){ // This function tells us if a button has been pressed, and which button
  if(digitalRead(buttonOne)==0){return 1;
  }else if(digitalRead(buttonTwo)==0){return 2;
  }else if(digitalRead(buttonThree)==0){return 3;
  }else{return 0;}
}

void drawchar(int x,int y,char ch, uint8_t color) {
  if (color == WHITE){
    display.fillRect(x,y,5,6,BLACK);
  }else{
    display.fillRect(x,y,5,6,WHITE);
  }
  display.setTextSize(1);
  display.setTextColor(color);
  display.setCursor(x,y);
  display.write(ch);  
  display.display();
}

void writestring(int x, int y, char string[], uint8_t color){
  for(int i=0; i<strlen(string); i+=1){
    drawchar(x+6*i,y,string[i],color);
  }
}


